$(document).ready(function() {
	// for drawer menu on mobile
    $(".navbar-toggle").click(function(){
        $("#navbar").toggleClass('in');
    });

	
	// Bootstrap dropdown menu on hover
	$('ul.nav li.dropdown').hover(function() {
	  	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(200);
	}, function() {
	  	$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(200);
	});



	// Go to pricing table on click of buy now button
	$("#go-to-table").click(function() {
	    $('html,body').animate({
	        scrollTop: $(".pricing-section").offset().top},
	        'slow');
	});



	// Testimonial slider homapage
    var owl = $('.testimonial');
	owl.owlCarousel({
	    items: 2,
	    loop: true,
	    margin: 10,
	    autoplay: true,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: false,
	    responsive:{
	        0:{
	            items: 1
	        },
	        600:{
	            items: 2
	        },
	        1000:{
	            items: 2
	        }
	    }
	});



	// jQuery counter up
	// $('.counter').counterUp({
 //        delay: 10,
 //        time: 1000
 //    });


	// (function(){
	// 	$('#carousel123').carousel({ interval: 200000 });
		  
	// 	$('.carousel-showsixmoveone .item').each(function(){
	// 	    var itemToClone = $(this);

	// 	    for (var i=1;i<6;i++) {
	// 	      itemToClone = itemToClone.next();

	// 	      // wrap around if at end of item collection
	// 	      if (!itemToClone.length) {
	// 	        itemToClone = $(this).siblings(':first');
	// 	      }

	// 	      // grab item, clone, add marker class, add to collection
	// 	      itemToClone.children(':first-child').clone()
	// 	        .addClass("cloneditem-"+(i))
	// 	        .appendTo($(this));
	// 	    }
	// 	});
	// }());


	//           $(document).on('click', '[data-toggle="lightbox"]:not([data-gallery="navigateTo"]):not([data-gallery="example-gallery-11"])', function(event) {
    //               event.preventDefault();
    //               return $(this).ekkoLightbox({
    //                   onShown: function() {
    //                       if (window.console) {
    //                           return console.log('Checking our the events huh?');
    //                       }
    //                   },
						// onNavigate: function(direction, itemIndex) {
    //                       if (window.console) {
    //                           return console.log('Navigating '+direction+'. Current item: '+itemIndex);
    //                       }
						// }
    //               });
    //           });



});


// Scroll fix header
$(window).scroll(function(){
    if($(window).width() >= 991){
        if ($(window).scrollTop() >= 200) {
               $('.sticky-wrapper').addClass('fixed-header animated fadeInDown');
        }
        else {
               $('.sticky-wrapper').removeClass('fixed-header animated fadeInDown');
        }
    }
});



